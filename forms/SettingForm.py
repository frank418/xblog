#-*- coding:UTF-8 -*-
__author__ = 'Franke Yang <frankyang418@gmail.com>'
__date__ = "13-4-9 上午8:15"
__doc__ = u"系统设置表单"

from flask_wtf import Form, TextField, Required, IntegerField, QuerySelectField
from models.Themes import Themes


class SettingForm(Form):
    title = TextField(label=u"站点标题", validators=[Required(message=u"标题必须填写")])
    slogan = TextField(label=u"站点口号", validators=[Required(message=u"口号必须填写")])
    page_size = IntegerField(label=u"页面显示数", validators=[Required(message=u"必须填写")])
    theme = QuerySelectField(label=u"主题", query_factory=lambda: Themes.query.all(), get_label=lambda x: x.name, get_pk=lambda x: x.tid)