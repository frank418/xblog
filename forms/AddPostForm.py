#-*- coding:UTF-8 -*-
__author__ = 'Franke Yang <frankyang418@gmail.com>'
__date__ = "13-4-7 下午3:11"
__doc__ = u"添加文章表单"

from flask_wtf import Form, TextField, QuerySelectField, TextAreaField, HiddenField, Required, Optional
from models.Category import Category


class AddPostForm(Form):
    title = TextField(label=u"文章标题", validators=[Required(message=u"标题必须填写")])
    short_url = TextField(label=u"自定短路径", validators=[Required(message=u"标题必须填写")])
    category = QuerySelectField(label=u"文章分类", validators=[Required(message=u"分类必须选择")],
                                query_factory=lambda: Category.query.all(), get_pk=lambda x: x.cid, get_label=lambda x: x.label)
    summary = TextAreaField(label=u"文章摘要", validators=[Required(message=u"摘要必须填写")])
    content = TextAreaField(label=u"文章内容", validators=[Required(message=u"内容必须填写")])
    hidden_tags = HiddenField(label=u"文章标签", validators=[Optional()])
    hidden_atts = HiddenField(label=u"文章附件", validators=[Optional()])