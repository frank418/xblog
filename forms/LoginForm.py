#-*- coding:UTF-8 -*-
__author__ = 'Franke Yang <frankyang418@gmail.com>'
__date__ = "13-4-7 下午3:11"
__doc__ = u"登录表单"

from flask_wtf import Form, TextField, PasswordField, Required


class LoginForm(Form):
    uid = TextField(label=u"用户名", validators=[Required(message=u"用户名必须填写")])
    pwd = PasswordField(label=u"密码", validators=[Required(message=u"密码必须填写")])