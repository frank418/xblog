#-*- coding:UTF-8 -*-
__author__ = 'Franke Yang <frankyang418@gmail.com>'
__date__ = "13-4-8 上午9:39"
__doc__ = u"添加分类表单模型"

from flask_wtf import Form, TextField, Required, IntegerField, Optional


class AddCategoryForm(Form):
    name = TextField(u"标识名（英文）", validators=[Required(message=u"标识名必须提供")])
    label = TextField(u"分类名（中英文均可）", validators=[Required(message=u"分类名必须提供")])
    order = IntegerField(u"排序", validators=[Optional()])