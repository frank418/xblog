#-*- coding:UTF-8 -*-
__author__ = 'Franke Yang <frankyang418@gmail.com>'
__date__ = "13-4-8 下午6:52"
__doc__ = u"修改密码表单"

from flask_wtf import Form, TextField, PasswordField, Required, EqualTo


class ChangePasswordForm(Form):
    old_pass = PasswordField(label=u"旧密码", validators=[Required(u"旧密码必须输入")])
    new_pass = TextField(label=u"新密码", validators=[Required(u"新密码必须输入")])
    confirm_pass = TextField(label=u"重复新密码", validators=[Required(u"重复新密码必须输入"), EqualTo('new_pass', u"两次密码输入不一致")])