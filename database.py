#-*- coding:UTF-8 -*-
__author__ = 'Franke Yang <frankyang418@gmail.com>'
__date__ = "13-4-4 下午1:38"
__doc__ = u"数据库连接"

from sqlalchemy import create_engine
from sqlalchemy.orm import scoped_session, sessionmaker
from sqlalchemy.ext.declarative import declarative_base

conn_str = "mysql://root:root@127.0.0.1/0418_me?charset=utf8"
engine = create_engine(conn_str, convert_unicode=True, echo=False)
db_session = scoped_session(sessionmaker(bind=engine))
Base = declarative_base()
Base.query = db_session.query_property()