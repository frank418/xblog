#-*- coding:UTF-8 -*-
from flask import Flask, g, send_from_directory
from flaskext.markdown import Markdown

app = Flask(__name__)
Markdown(app)
from controllers.home import bpBlog
from controllers.manager import bpManager

app.register_blueprint(bpBlog)
app.register_blueprint(bpManager)
app.secret_key = '07AC133E2A3BED56782DB04E50FE8835'
import os
from models.Setting import Setting
from models.Themes import Themes
from models.Blog import Blog
from models.User import User
from flask import get_flashed_messages, render_template, request
import PyRSS2Gen
import datetime
import time


@app.before_request
def before_request():
    setting = Setting.query.first()
    g.title = setting.title
    g.slogan = setting.slogan
    g.page_size = setting.page_size
    g.theme_path = Themes.query.filter(Themes.tid == setting.tid).first().path


@app.route('/favicon.ico')
def favicon():
    return send_from_directory(os.path.join(app.root_path, 'static', 'img'), 'favicon.ico', mimetype='image/vnd.microsoft.icon')


@app.route('/<path>.txt')
def txt(path):
    return send_from_directory(os.path.join(app.root_path, 'static'), "%s.txt" % path)


@app.errorhandler(404)
def page_not_found(e):
    tpl_path = 'home/%s/404.html' % g.theme_path
    return render_template(tpl_path), 404


@app.route('/deny')
def deny():
    errors = get_flashed_messages()
    return errors[0]


@app.template_filter()
def timesince(dt, default=None):
    if default is None:
        default = u"Just Now"

    now = datetime.datetime.now()
    diff = now - dt

    years = diff.days / 365
    months = diff.days / 30
    weeks = diff.days / 7
    days = diff.days
    hours = diff.seconds / 3600
    minutes = diff.seconds / 60
    seconds = diff.seconds

    periods = (
        (years, u"%(num)s years" % dict(num=years)),
        (months, u"%(num)s month" % dict(num=months)),
        (weeks, u"%(num)s week" % dict(num=weeks)),
        (days, u"%(num)s days" % dict(num=days)),
        (hours, u"%(num)s hours" % dict(num=hours)),
        (minutes, u"%(num)s minutes" % dict(num=minutes)),
        (seconds, u"%(num)s seconds" % dict(num=seconds)),
    )

    for period, trans in periods:
        if period:
            return u"%(period)s ago" % dict(period=trans)

    return default


@app.route('/feed')
def feed():
    rss_file = 'rss.xml'
    file_mtime = os.stat(os.path.join(app.root_path, 'static', rss_file)).st_mtime
    now = time.mktime(datetime.datetime.now().timetuple())
    if (now - file_mtime) > 60.0:
        setting = Setting.query.first()
        rss = PyRSS2Gen.RSS2(title=setting.title,
                             link=request.host_url,
                             description=setting.slogan)
        rss.lastBuildDate = datetime.datetime.now()
        blogs = Blog.query.order_by(Blog.edit_date.desc()).offset(0).limit(10).all()

        for blog in blogs:
            item = PyRSS2Gen.RSSItem(title=blog.title)
            item.link = "%s%s.html" % (request.host_url, blog.short_url)
            item.pubDate = blog.edit_date
            item.author = User.query.filter(User.uid == blog.author).first().real_name
            item.description = blog.summary
            rss.items.append(item)
        rss.write_xml(open(os.path.join(app.root_path, 'static', rss_file), 'w'), encoding='UTF-8')
    return send_from_directory(os.path.join(app.root_path, 'static'), rss_file, mimetype='application/rss+xml')


if __name__ == '__main__':
    print app.url_map
    app.debug = True
    app.run()
