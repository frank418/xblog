#-*- coding:UTF-8 -*-
__author__ = 'Franke Yang <frankyang418@gmail.com>'
__date__ = "13-4-7 下午2:48"
__doc__ = u""

from sqlalchemy import Column, Integer, String

from database import Base


class Category(Base):
    u"""
    分类模型
    """
    __tablename__ = 'category'
    cid = Column(Integer, primary_key=True)
    name = Column(String(length=20))
    label = Column(String(length=20))
    order = Column(Integer, default=0)

    def __init__(self, name, label):
        self.name = name
        self.label = label