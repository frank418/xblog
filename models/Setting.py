#-*- coding:UTF-8 -*-
__author__ = 'Franke Yang <frankyang418@gmail.com>'
__date__ = "13-4-7 下午2:47"
__doc__ = u""

from sqlalchemy import Column, Integer, String, ForeignKey

from database import Base


class Setting(Base):
    u"""
    系统设置模型
    """
    __tablename__ = 'settings'
    sid = Column(Integer, primary_key=True)
    title = Column(String(length=100))
    slogan = Column(String(length=100))
    url = Column(String(length=100))
    page_size = Column(Integer, default=10)
    tid = Column(Integer, ForeignKey('themes.tid'))