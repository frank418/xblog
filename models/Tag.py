#-*- coding:UTF-8 -*-
__author__ = 'Franke Yang <frankyang418@gmail.com>'
__date__ = "13-4-7 下午2:48"
__doc__ = u""
from sqlalchemy import Column, Integer, String

from database import Base


class Tag(Base):
    u"""
    标签模型
    """
    __tablename__ = 'tags'
    tid = Column(Integer, primary_key=True)
    tag_name = Column(String(length=20))

    def __init__(self, tag_name):
        self.tag_name = tag_name