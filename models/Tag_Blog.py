#-*- coding:UTF-8 -*-
__author__ = 'Franke Yang <frankyang418@gmail.com>'
__date__ = "13-4-7 下午2:50"
__doc__ = u""
from sqlalchemy import Column, Integer, ForeignKey

from database import Base


class Tag_Blog(Base):
    """
    标签和博客文章关系表
    """
    __tablename__ = 'tag_blog'
    tb_id = Column(Integer, primary_key=True)
    tag_id = Column(Integer, ForeignKey('tags.tid'))
    blog_id = Column(Integer, ForeignKey('blogs.bid'))