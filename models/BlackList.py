#-*- coding:UTF-8 -*-
__author__ = 'Franke Yang <frankyang418@gmail.com>'
__date__ = "13-4-4 下午1:38"

from sqlalchemy import Column, Integer, DateTime

from database import Base


class BlackList(Base):
    u"""
    黑名单模型
    """
    __tablename__ = 'blacklists'
    bid = Column(Integer, primary_key=True)
    ip = Column(Integer)
    expire_time = Column(DateTime)