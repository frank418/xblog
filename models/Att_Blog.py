#-*- coding:UTF-8 -*-
__author__ = 'Franke Yang <frankyang418@gmail.com>'
__date__ = "13-4-7 下午2:49"
__doc__ = u""

from sqlalchemy import Column, Integer, ForeignKey

from database import Base


class Att_Blog(Base):
    u"""
    附件与博客文章关系表
    """
    __tablename__ = 'att_blog'
    ab_id = Column(Integer, primary_key=True)
    att_id = Column(Integer, ForeignKey('attachments.aid'))
    blog_id = Column(Integer, ForeignKey('blogs.bid'))