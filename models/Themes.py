#-*- coding:UTF-8 -*-
__author__ = 'Franke Yang <frankyang418@gmail.com>'
__date__ = "13-4-7 下午2:46"
__doc__ = u""

from sqlalchemy import Column, Integer, String, Text

from database import Base


class Themes(Base):
    u"""
    主题模型
    """
    __tablename__ = 'themes'
    tid = Column(Integer, primary_key=True)
    name = Column(String(length=50))
    path = Column(String(length=50))
    author = Column(String(length=100))
    description = Column(Text)