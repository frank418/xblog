#-*- coding:UTF-8 -*-
__author__ = 'Franke Yang <frankyang418@gmail.com>'
__date__ = "13-4-7 下午2:40"
__doc__ = u""

from sqlalchemy import Column, Integer, String
from database import Base


class User(Base):
    u"""
    用户模型
    """
    __tablename__ = 'users'
    uid = Column(Integer, primary_key=True)
    login_name = Column(String(length=40))
    real_name = Column(String(length=40))
    pwd = Column(String(length=32))
    email = Column(String(length=60))