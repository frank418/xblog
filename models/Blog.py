#-*- coding:UTF-8 -*-
__author__ = 'Franke Yang <frankyang418@gmail.com>'
__date__ = "13-4-7 下午2:51"
__doc__ = u""

from sqlalchemy import Column, Integer, String, Text, ForeignKey, DateTime
from sqlalchemy.orm import relationship

from database import Base
from models.Tag import Tag
from models.Tag_Blog import Tag_Blog
from models.Att_Blog import Att_Blog
from models.Attachment import Attachment


class Blog(Base):
    """
    博客文章模型
    """
    __tablename__ = 'blogs'
    bid = Column(Integer, primary_key=True, autoincrement=10000)
    title = Column(String(length=200))
    short_url = Column(String(length=50))
    author = Column(Integer, ForeignKey('users.uid'))
    tags = relationship(Tag, backref='blogs', secondary=Tag_Blog.__table__)
    category = Column(Integer, ForeignKey('category.cid'))
    summary = Column(String(length=200))
    content = Column(Text)
    attachments = relationship(Attachment, backref='blogs', secondary=Att_Blog.__table__)
    post_date = Column(DateTime)
    edit_date = Column(DateTime)