#-*- coding:UTF-8 -*-
__author__ = 'Franke Yang <frankyang418@gmail.com>'
__date__ = "13-4-7 下午2:51"
__doc__ = u""

from sqlalchemy import Column, Integer, Text

from database import Base


class Attachment(Base):
    """
    附件模型
    """
    __tablename__ = 'attachments'
    aid = Column(Integer, primary_key=True)
    path = Column(Text)
    url = Column(Text)