#-*- coding:UTF-8 -*-
__author__ = 'Franke Yang <frankyang418@gmail.com>'
__date__ = "13-4-8 上午8:30"
__doc__ = u"模板辅助工具"

import datetime

from flask import render_template, g


def render(tpl_path, **variables):
    process_time = (datetime.datetime.now() - g.start_time).total_seconds()
    varis = variables.copy()
    varis['process_time'] = process_time
    return render_template(tpl_path, **varis)