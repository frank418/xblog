#-*- coding:UTF-8 -*-
__author__ = 'Franke Yang <frankyang418@gmail.com>'
__date__ = "13-4-7 下午2:56"
__doc__ = u"md5加密方法"

import hashlib


def md5(src):
    return hashlib.md5(src).hexdigest().upper()