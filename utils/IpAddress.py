#-*- coding:UTF-8 -*-
__author__ = 'Franke Yang <frankyang418@gmail.com>'
__date__ = "13-4-7 下午3:01"
__doc__ = u"ip地址操作工具"

import struct
import socket


def str2uint(_str):
# 得到始终是正数
    return socket.ntohl(struct.unpack("I", socket.inet_aton(_str))[0])


def ip2int(_str):
    uint = socket.ntohl(struct.unpack("I", socket.inet_aton(_str))[0])
    # 先得到负数，再转换一下
    return struct.unpack("i", struct.pack('I', uint))[0]


def int2ip(_int):
    if _int < 0:
        _int = struct.unpack("I", struct.pack('i', _int))[0]
    return socket.inet_ntoa(struct.pack('I', socket.htonl(_int)))
