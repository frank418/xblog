#-*- coding:UTF-8 -*-
__author__ = 'Franke Yang <frankyang418@gmail.com>'
__date__ = "13-4-16 上午8:24"
__doc__ = u""
from  tornado.wsgi import WSGIContainer
from tornado.httpserver import HTTPServer
from tornado.ioloop import IOLoop
from me_0418 import app
import sys

if __name__ == '__main__':
    if len(sys.argv) == 2:
        port = int(sys.argv[1])
        http_server = HTTPServer(WSGIContainer(app))
        http_server.listen(port, address='127.0.0.1')
        IOLoop.instance().start()
    else:
        print "Usage $./run_server $port"