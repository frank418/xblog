#-*- coding:UTF-8 -*-
__author__ = 'Franke Yang <frankyang418@gmail.com>'
__date__ = "13-4-4 下午2:34"
__doc__ = u"初始化程序"

from utils.md5 import md5

if __name__ == '__main__':
    from database import db_session, Base, engine
    import models.Themes
    import models.Attachment
    import models.Blog
    import models.Att_Blog
    import models.BlackList
    import models.Setting
    import models.Category
    import models.Tag
    import models.Tag_Blog
    import models.User

    Base.metadata.drop_all(bind=engine)
    Base.metadata.create_all(bind=engine)
    from models.Themes import Themes

    default_theme = Themes()
    default_theme.name = u'默认'
    default_theme.path = 'default'
    default_theme.author = 'Frank Yang <frankyang418@gmail.com>'
    default_theme.description = u"默认自带的主题"
    db_session.add(default_theme)
    metro_theme = Themes()
    metro_theme.name = u'Metro'
    metro_theme.path = 'metro'
    metro_theme.author = 'Frank Yang <frankyang418@gmail.com>'
    default_theme.description = u'Metro 主题'
    db_session.commit()
    from models.Setting import Setting

    setting = Setting()
    setting.title = u"XBlog"
    setting.slogan = u"Yet Another Python  Blog."
    setting.page_size = 10
    setting.tid = default_theme.tid
    setting.url = 'https://0418.me'
    db_session.add(setting)
    db_session.commit()
    from models.Category import Category

    cate = Category('default', "默认分类")
    db_session.add(cate)
    db_session.commit()
    from models.Tag import Tag

    tag = Tag('default')
    db_session.add(tag)
    db_session.commit()
    from models.User import User

    user = User()
    user.login_name = '用户名'
    user.email = '邮箱地址'
    user.pwd = md5('密码')
    user.real_name = u"显示名"
    db_session.add(user)
    db_session.commit()
else:
    print 'Usage:python init.py'