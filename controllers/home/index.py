#-*- coding:UTF-8 -*-
__author__ = 'Franke Yang <frankyang418@gmail.com>'
__date__ = "13-4-7 下午4:23"
__doc__ = u"首页"

from flask import g
from flask_sqlalchemy import Pagination

from controllers.home import bpBlog
from models.Blog import Blog
from utils.Template import render


@bpBlog.route('/')
@bpBlog.route('/<int:page_no>')
def index(page_no=1):
    page_size = g.page_size
    blogs = Blog.query.order_by(Blog.bid.desc()).offset(page_size * (page_no - 1)).limit(page_size).all()
    pagination = Pagination(Blog.query.order_by(Blog.bid.desc()).all(), page_no, page_size, Blog.query.count(), items=None)
    tpl_path = 'home/%s/index.html' % g.theme_path
    return render(tpl_path, pagination=pagination, blogs=blogs)