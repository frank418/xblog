#-*- coding:UTF-8 -*-
__author__ = 'Franke Yang <frankyang418@gmail.com>'
__date__ = "13-4-7 下午5:56"
__doc__ = u""

from flask import render_template, g, flash
from flask_sqlalchemy import Pagination

from controllers.home import bpBlog
from models.Blog import Blog
from models.Category import Category
from utils.Template import render
from models.User import User


@bpBlog.route('/c/<int:cid>')
def view_by_cate_id(cid):
    blogs = Blog.query.filter(Blog.category == cid).all()
    tpl_path = 'home/%s/list.html' % g.theme_path
    return render_template(tpl_path, blogs=blogs)


@bpBlog.route('/t/<tag_name>')
def view_by_tag_name(tag_name):
    pass


@bpBlog.route('/c/<cate_name>')
@bpBlog.route('/c/<cate_name>/<int:page_no>')
def view_by_cate_name(cate_name, page_no=1):
    category = Category.query.filter(Category.name == cate_name).first()
    if category:
        page_size = g.page_size
        tpl_path = 'home/%s/list.html' % g.theme_path
        blogs = Blog.query.filter(Blog.category == category.cid).order_by(Blog.bid.desc()).offset(page_size * (page_no - 1)).limit(page_size).all()
        pagination = Pagination(blogs, page_no, page_size, Blog.query.filter(Blog.category == category.cid).count(), items=None)
        return render(tpl_path, blogs=blogs, category=category, pagination=pagination)
    else:
        flash(u"没有这个分类！", category='error')
        tpl_path = 'home/%s/404.html' % g.theme_path
        return render_template(tpl_path)


@bpBlog.route('/p/<int:pid>')
def view_post_by_id(pid):
    pass


@bpBlog.route('/<short_url>.html')
def view_post_by_url(short_url):
    blog = Blog.query.filter(Blog.short_url == short_url).first()
    if blog:
        category = Category.query.filter(Category.cid == blog.category).first()
        author = User.query.filter(User.uid == blog.author).first()
        prev_blog = Blog.query.filter(Blog.bid == blog.bid - 1).first()
        next_blog = Blog.query.filter(Blog.bid == blog.bid + 1).first()
        tpl_path = 'home/%s/blog.html' % g.theme_path
        return render(tpl_path, blog=blog, next_blog=next_blog, prev_blog=prev_blog, category=category)
    tpl_path = 'home/%s/404.html' % g.theme_path
    return render_template(tpl_path)

