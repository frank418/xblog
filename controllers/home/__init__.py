#-*- coding:UTF-8 -*-
__author__ = 'Franke Yang <frankyang418@gmail.com>'
__date__ = "13-4-7 下午4:23"
__doc__ = u""

import datetime
import platform
import sys

from flask import Blueprint, g
import flask

from models.Category import Category
from models.Tag import Tag


bpBlog = Blueprint('bpBlog', 'bpBlog')


@bpBlog.before_request
def before_home_request():
    g.start_time = datetime.datetime.now()
    g.categories = Category.query.order_by(Category.order).all()
    g.tags = Tag.query.all()
    g.platform = "%s %s %s" % platform.linux_distribution()
    g.python = sys.version.split(' ')[0:1][0]
    g.flask = flask.__version__


import index
import view