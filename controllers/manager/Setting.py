#-*- coding:UTF-8 -*-
__author__ = 'Franke Yang <frankyang418@gmail.com>'
__date__ = "13-4-7 下午3:51"
__doc__ = u"系统设置控制器"

import platform
import sys
import os
import json
from flask import render_template, request, session, flash, current_app
from sqlalchemy.exc import SQLAlchemyError

from controllers.manager import bpManager
from decorators.LoginRequired import login_required
from forms.ChangePasswordForm import ChangePasswordForm
from forms.SettingForm import SettingForm
from models.User import User
from models.Setting import Setting
from models.Themes import Themes
from utils.md5 import md5
from database import db_session


@bpManager.route('/sys/info', methods=['GET', 'POST'])
@login_required
def sysinfo():
    form = SettingForm()
    setting = Setting.query.first()
    if request.method == 'GET':
        #读取主题
        tpl_path = os.path.join(current_app.root_path, current_app.template_folder, 'home')
        for x in os.listdir(tpl_path):
            print x
            ret = Themes.query.filter(Themes.path == x).first()
            if ret is None:
                theme_config = json.load(open("%s/%s/theme.json" % (tpl_path, x)), encoding='UTF-8')
                theme = Themes()
                theme.path = theme_config['path']
                theme.author = theme_config['author']
                theme.description = theme_config['description']
                theme.name = theme_config['name']
                try:
                    db_session.add(theme)
                    db_session.commit()
                except SQLAlchemyError, err:
                    db_session.rollback()
                    flash(err.message, category='error')

    if request.method == 'POST' and form.validate():
        setting.page_size = form.page_size.data
        setting.slogan = form.slogan.data
        setting.title = form.title.data
        setting.tid = form.theme.data.tid
        try:
            db_session.add(setting)
            db_session.commit()
            flash(u"修改成功！", category='success')
        except SQLAlchemyError, err:
            db_session.rollback()
            flash(u"修改失败！%s" % err.message, category='error')

    os_name = platform.platform()
    python_ver = sys.version
    return render_template('manager/sys/info.html', os_name=os_name, python_ver=python_ver, form=form, setting=setting)


@bpManager.route('/changepassword', methods=['POST', 'GET'])
@login_required
def changepassword():
    form = ChangePasswordForm()
    if request.method == 'POST' and form.validate():
        user = User.query.filter(User.login_name == session['current_user'].login_name, User.pwd == md5(form.old_pass.data)).first()
        if user:
            user.pwd = md5(form.new_pass.data)
            try:
                db_session.add(user)
                db_session.commit()
                flash(u"修改密码成功！", category='success')
            except SQLAlchemyError, err:
                db_session.rollback()
                flash(u"修改密码出错！%s" % err.message, category='error')
        else:
            flash(u"旧密码不正确，请重试！", category='error')
        return render_template('manager/sys/changepassword.html', form=form)
    else:
        return render_template('manager/sys/changepassword.html', form=form)
