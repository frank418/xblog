#-*- coding:UTF-8 -*-
__author__ = 'Franke Yang <frankyang418@gmail.com>'
__date__ = "13-4-7 下午3:16"
__doc__ = u"文章管理控制器"

import datetime

from flask import request, session, flash, redirect, url_for, render_template, g
from sqlalchemy.exc import SQLAlchemyError
from flask_sqlalchemy import Pagination

from controllers.manager import bpManager
from decorators.LoginRequired import login_required
from decorators.IsBlacklisted import is_blacklisted
from forms.AddPostForm import AddPostForm
from database import db_session
from models.Blog import Blog
from models.Tag import Tag
from models.Attachment import Attachment


@bpManager.route('/')
@bpManager.route('/post/add', methods=['POST', 'GET'])
@is_blacklisted
@login_required
def add_post():
    form = AddPostForm()
    if request.method == 'POST' and form.validate():
        blog = Blog()
        blog.author = session['current_user'].uid
        blog.category = form.category.data.cid
        blog.content = form.content.data
        blog.short_url = form.short_url.data
        blog.title = form.title.data
        blog.summary = form.summary.data
        blog.post_date = datetime.datetime.now()
        blog.edit_date = datetime.datetime.now()
        tag_ids = [int(x) for x in form.hidden_tags.data.split(',')[1:]]
        if len(tag_ids) > 0:
            for tag in Tag.query.filter(Tag.tid.in_(tag_ids)):
                blog.tags.append(tag)
        att_ids = [int(x) for x in form.hidden_atts.data.split(',')[1:]]
        if len(att_ids) > 0:
            for att in Attachment.query.filter(Attachment.aid.in_(att_ids)):
                blog.attachments.append(att)
        try:
            db_session.add(blog)
            db_session.commit()
            flash(u"文章添加成功！", category='success')
            return redirect(url_for('bpManager.add_post'))
        except SQLAlchemyError, err:
            db_session.rollback()
            flash(u"保存时出错！%s" % err.message, category='error')

    tags = Tag.query.all()
    return render_template('manager/post/add.html', form=form, tags=tags)


@bpManager.route('/post/edit/<int:bid>', methods=['POST', 'GET'])
@is_blacklisted
@login_required
def edit_post(bid):
    form = AddPostForm()
    if request.method == 'GET':
        b = Blog.query.filter(Blog.bid == bid).first()
        form.summary.data = b.summary
        form.content.data = b.content
        tags = Tag.query.all()
        return render_template('manager/post/edit.html', blog=b, form=form, tags=tags)
    elif request.method == 'POST' and form.validate():
        blog = Blog.query.filter(Blog.bid == bid).first()
        blog.author = session['current_user'].uid
        blog.category = form.category.data.cid
        blog.content = form.content.data
        blog.short_url = form.short_url.data
        blog.title = form.title.data
        blog.summary = form.summary.data
        blog.edit_date = datetime.datetime.now()
        tag_ids = [int(x) for x in form.hidden_tags.data.split(',')[1:]]
        if len(tag_ids) > 0:
            for tag in Tag.query.filter(Tag.tid.in_(tag_ids)):
                blog.tags.append(tag)
        att_ids = [int(x) for x in form.hidden_atts.data.split(',')[1:]]
        if len(att_ids) > 0:
            for att in Attachment.query.filter(Attachment.aid.in_(att_ids)):
                blog.attachments.append(att)
        try:
            db_session.add(blog)
            db_session.commit()
            flash(u"文章编辑成功！", category='success')
        except SQLAlchemyError, err:
            db_session.rollback()
            flash(u"文章编辑时出错！%s" % err.message, category='error')
        return redirect(url_for('bpManager.edit_post', bid=bid))


@bpManager.route('/post/del/<int:bid>', methods=['GET'])
@is_blacklisted
@login_required
def del_post(bid):
    blog = Blog.query.filter(Blog.bid == bid).first()
    if blog:
        return render_template('manager/post/del_post.html', blog=blog)


@bpManager.route('/post/del/<int:bid>/confirm', methods=['POST'])
@is_blacklisted
@login_required
def confirm_del(bid):
    blog = Blog.query.filter(Blog.bid == bid).first()
    if blog:
        try:
            db_session.delete(blog)
            db_session.commit()
            flash(u"删除文章成功！", category='success')
        except SQLAlchemyError, err:
            flash(u"删除文章失败！%s" % err.message, category='error')
    return redirect(url_for('bpManager.posts', page=1))


@bpManager.route('/post/list/<int:page>')
@is_blacklisted
@login_required
def posts(page=1):
    page_size = g.page_size
    blogs = Blog.query.order_by(Blog.bid.desc()).offset(page_size * (page - 1)).limit(page_size).all()
    pagination = Pagination(Blog.query.order_by(Blog.bid.desc()).all(), page, page_size, Blog.query.count(), items=None)
    return render_template('manager/post/list.html', pagination=pagination, blogs=blogs)
