#-*- coding:UTF-8 -*-
__author__ = 'Franke Yang <frankyang418@gmail.com>'
__date__ = "13-4-7 下午4:01"
__doc__ = u""

from flask import render_template, flash, redirect, url_for
from sqlalchemy.exc import SQLAlchemyError

from controllers.manager import bpManager
from decorators.LoginRequired import login_required
from models.Category import Category
from database import db_session
from forms.AddCategoryForm import AddCategoryForm


@bpManager.route('/category/add', methods=['POST'])
@login_required
def add_category():
    form = AddCategoryForm()
    if form.validate():
        category = Category(form.name.data, form.label.data)
        try:
            db_session.add(category)
            db_session.commit()
            flash(u"分类添加成功！", category='success')
        except SQLAlchemyError, err:
            db_session.rollback()
            flash(u"分类添加失败！%s" % err.message, category='error')
    return redirect(url_for('bpManager.categories'))


@bpManager.route('/category/edit/<int:cid>', methods=['POST'])
@login_required
def edit_category(cid):
    form = AddCategoryForm()
    if form.validate():
        category = Category.query.filter(Category.cid == cid).first()
        if not category:
            flash(u"修改分类失败！没有这个分类！", category='error')
        else:
            category.label = form.label.data
            category.name = form.name.data
            category.order = form.order.data
            try:
                db_session.add(category)
                db_session.commit()
                flash(u"修改分类成功！", category='success')
            except SQLAlchemyError, err:
                flash(u"分类修改失败！%s" % err.message, category='error')
                db_session.rollback()
    return redirect(url_for('bpManager.categories'))


@bpManager.route('/category/list')
@login_required
def categories():
    categories = Category.query.order_by(Category.cid.desc()).all()
    form = AddCategoryForm()
    return render_template('manager/category/list.html', categories=categories, form=form)


@bpManager.route('/category/del/<int:cid>')
@login_required
def del_category(cid):
    ret = Category.query.filter(Category.cid == cid).first()
    if ret:
        try:
            db_session.delete(ret)
            db_session.commit()
            flash(u"删除分类成功！", category='success')
        except SQLAlchemyError, err:
            db_session.rollback()
            flash(u"删除分类出错！%s" % err.message, category='error')
    else:
        flash(u"没有这个分类！", category='error')
    return redirect(url_for('bpManager.categories'))
