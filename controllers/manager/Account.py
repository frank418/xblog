#-*- coding:UTF-8 -*-
__author__ = 'Franke Yang <frankyang418@gmail.com>'
__date__ = "13-4-7 下午3:09"
__doc__ = u"帐号控制器"

import datetime

from flask import request, session, redirect, url_for, flash, render_template
from sqlalchemy.exc import SQLAlchemyError

from controllers.manager import bpManager
from decorators.IsBlacklisted import is_blacklisted
from decorators.LoginRequired import login_required
from forms.LoginForm import LoginForm
from database import db_session
from models.User import User
from models.BlackList import BlackList
from utils.md5 import md5
from utils.IpAddress import ip2int


@bpManager.route('/login', methods=['POST', 'GET'])
@is_blacklisted
def login():
    login_form = LoginForm()
    if request.method == 'POST' and login_form.validate():
        user = User.query.filter(User.login_name == login_form.uid.data, User.pwd == md5(login_form.pwd.data)).first()
        if user:
            db_session.merge(user)
            session['current_user'] = user
            return redirect(url_for('bpManager.sysinfo'))
        else:
            if 'login_count' in session:
                if session['login_count'] > 2:
                    black = BlackList.query.filter(BlackList.ip == ip2int(request.remote_addr)).first()
                    if not black:
                        black = BlackList()
                    black.expire_time = datetime.datetime.now() + datetime.timedelta(seconds=120)
                    black.ip = ip2int(request.remote_addr)
                    try:
                        db_session.add(black)
                        db_session.commit()
                    except SQLAlchemyError:
                        db_session.rollback()
                    finally:
                        db_session.close()
                session['login_count'] += 1
            else:
                session['login_count'] = 1
            flash(u"用户名或密码错误！", category='error')

    return render_template('manager/login.html', form=login_form)


@bpManager.route('/exit')
@login_required
def logout():
    session.clear()
    return redirect(url_for('bpManager.login'))