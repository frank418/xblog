#-*- coding:UTF-8 -*-
__author__ = 'Franke Yang <frankyang418@gmail.com>'
__date__ = "13-4-7 下午4:48"
__doc__ = u"附件控制器"

from controllers.manager import bpManager
from decorators.LoginRequired import login_required


@bpManager.route('/attachment/list')
@login_required
def list_attachment():
    pass