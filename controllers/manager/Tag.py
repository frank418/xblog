#-*- coding:UTF-8 -*-
__author__ = 'Franke Yang <frankyang418@gmail.com>'
__date__ = "13-4-7 下午3:24"
__doc__ = u""

from sqlalchemy.exc import SQLAlchemyError
from flask import request

from controllers.manager import bpManager
from decorators.LoginRequired import login_required
from decorators.IsBlacklisted import is_blacklisted
from database import db_session
from models.Tag import Tag


@bpManager.route('/tag/add', methods=['POST'])
@is_blacklisted
@login_required
def add_tag():
    tag = Tag(request.form['tag_name'])
    try:
        db_session.add(tag)
        db_session.commit()
        tags = Tag.query.all()
        ret_str = ''
        for t in tags:
            ret_str += "<li value='%d' id='btn_tag_%d' class='btn btn-mini'>%s</li>" % (t.tid, t.tid, t.tag_name)
    except SQLAlchemyError, err:
        db_session.rollback()
        ret_str = u"添加标签时出错，%s" % err.message

    return ret_str


@bpManager.route('/tag/list')
def list_tag():
    pass