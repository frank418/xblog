#-*- coding:UTF-8 -*-
__author__ = 'Franke Yang <frankyang418@gmail.com>'
__date__ = "13-4-7 下午2:53"
__doc__ = u""
import os

from flask import Blueprint, g


bpManager = Blueprint('bpManager', 'bpManager', url_prefix='/x')


@bpManager.before_request
def before_admin_request():
    from me_0418 import app

    g.UPLOAD_PATH = os.path.join(app.root_path, 'static', 'uploads')


import Account
import File
import Post
import Tag
import Setting
import Category
import Attachment