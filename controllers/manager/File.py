#-*- coding:UTF-8 -*-
__author__ = 'Franke Yang <frankyang418@gmail.com>'
__date__ = "13-4-7 下午2:54"
__doc__ = u"文件管理控制器"

import datetime
import os
import json

from flask import request, g
from werkzeug.utils import secure_filename
from sqlalchemy.exc import SQLAlchemyError

from controllers.manager import bpManager
from decorators.LoginRequired import login_required
from models.Attachment import Attachment
from database import db_session


@bpManager.route('/file/upload', methods=['POST'])
@login_required
def file_upload():
    upfile = request.files['filename']
    file_name = "%s_%s" % (datetime.datetime.now().strftime('%Y_%m_%d_%H_%M_%S'), secure_filename(upfile.filename))
    path_save_file = os.path.join(g.UPLOAD_PATH, file_name)
    upfile.save(path_save_file)
    url_save_file = '/%s/%s/%s' % ('static', 'uploads', file_name)
    att = Attachment()
    att.url = url_save_file
    att.path = path_save_file
    try:
        db_session.add(att)
        db_session.commit()
        ret = {'fileid': att.aid, 'filename': url_save_file}
    except SQLAlchemyError:
        db_session.rollback()
        ret = {'fileid': 0, 'filename': u"上传文件时发生错误！"}
    return json.dumps(ret)


@bpManager.route('/file/delete', methods=['GET'])
@bpManager.route('/file/delete/<int:aid>', methods=['GET'])
@login_required
def file_delete(aid):
    ret = Attachment.query.filter(Attachment.aid == aid).first()
    try:
        pre_del_path = ret.path
        os.remove(pre_del_path)
        db_session.delete(ret)
        db_session.commit()
        ret_str = {'astatus': 1, 'amessage': u"附件删除成功！"}
    except Exception, err:
        db_session.rollback()
        ret_str = {'astatus': 0, 'amessage': err.message}

    return json.dumps(ret_str)