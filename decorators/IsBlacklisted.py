#-*- coding:UTF-8 -*-
__author__ = 'Franke Yang <frankyang418@gmail.com>'
__date__ = "13-4-7 下午2:59"
__doc__ = u"检测某个ip是否在黑名单中"
from functools import wraps
import datetime

from flask import flash, redirect, url_for, request

from models.BlackList import BlackList
from utils.IpAddress import ip2int


def is_blacklisted(func):
    @wraps(func)
    def decorated(*args, **kwargs):
        black_list = BlackList.query.filter(BlackList.ip == ip2int(request.remote_addr)).first()
        if black_list:
            now = datetime.datetime.now()
            offset = (black_list.expire_time - now).total_seconds()
            #print "*" * 9
            # offset
            if offset > 0:
                flash(u"您已经被禁止访问，解禁时间：%s" % black_list.expire_time, category='error')
                return redirect(url_for('deny'))
        return func(*args, **kwargs)

    return decorated