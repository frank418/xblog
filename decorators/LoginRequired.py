#-*- coding:UTF-8 -*-
__author__ = 'Franke Yang <frankyang418@gmail.com>'
__date__ = "13-4-7 下午2:57"
__doc__ = u""

from functools import wraps
from flask import flash, session, redirect, url_for


def login_required(func):
    @wraps(func)
    def decorated(*args, **kwargs):
        if 'current_user' not in session:
            flash(message=u"您还未登录！", category='error')
            return redirect(url_for("bpManager.login"))
        return func(*args, **kwargs)

    return decorated